/**
 *  report_issue_detail_widget.dart
 *  CouncilCentral
 *
 *  Created by ApplicationsTeam.
 *  Copyright © 2020 CouncilCentral. All rights reserved.
 */

import 'package:council_central/confirmation_submit_widget/confirmation_submit_widget.dart';
import 'package:council_central/report_issue_detail_widget/view_item_widget.dart';

/*import 'package:council_central/report_issue_detail_widget/view_three_item_widget.dart';
import 'package:council_central/report_issue_detail_widget/view_two_item_widget.dart';*/ // This will be used in the future.
import 'package:council_central/values/values.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:async';
import 'package:council_central/council_finder.dart';
import 'package:http/http.dart' as http;

class ReportIssueDetailWidget extends StatefulWidget {
  final File takenPhoto;

  ReportIssueDetailWidget({Key key, @required this.takenPhoto})
      : super(key: key);

  createState() => _ReportIssueDetailWidgetState();
}

class _ReportIssueDetailWidgetState extends State<ReportIssueDetailWidget> {
  List<File> _photos = [];

  var name;

  Future<void> onButtonPressed(BuildContext context) async {
    File pickedImage = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (pickedImage != null) {
      setState(() {
        _photos.add(pickedImage);
      });
    }
  }

  void onButtonTwoPressed(BuildContext context) => Navigator.push(context,
      MaterialPageRoute(builder: (context) => ConfirmationSubmitWidget()));

  void onItemPressed(BuildContext context) => Navigator.pop(context);

  Future<String> submitIssue(
      String name, String issue, String additionalDetails) async {
    final String apiUrl =
        "https://councilcentral1.atlassian.net/rest/api/3/issue/";
    final response = await http.post(apiUrl, headers: {
      "Content-Type": "application/json",
      "Authorization":
          "Basic ZGFuaWVsLmQub3Ntb25kQHN0dWRlbnQudXRzLmVkdS5hdTpISFlmZm15ZmYzMVJnVFZuRUxxazQ4Q0I="
    }, body: """{
  "fields": {

      "summary": "$name has created an issue",
      "issuetype": {
        "id": "10001"
      },
      "project": {
        "id": "10000"
      },
      "description": {
      "type": "doc",
      "version": 1,
      "content": [
        {
          "type": "paragraph",
          "content": [
            {
              "text": "Issue: $issue, Additional Details: $additionalDetails",
              "type": "text"
            }
          ]
        }
      ]
    }
  }
}""");
    final String responseString = response.body;

    if (response.statusCode == 201) {
      print("Success!");
      return responseString;
    } else {
      print("Failed to create issue");
      print(response.statusCode);
      print(responseString);
      return null;
    }
  }

  @override
  void initState() {
    _photos.add(widget.takenPhoto);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final TextEditingController nameController = TextEditingController();
    final TextEditingController issueController = TextEditingController();
    final TextEditingController additionalDetailsController =
        TextEditingController();

    return new Scaffold(
      appBar: AppBar(
        title: Text(
          "Report Issue",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Color.fromARGB(255, 255, 255, 255),
            fontFamily: "Verdana",
            fontWeight: FontWeight.w400,
            fontSize: 17,
          ),
        ),
        leading: IconButton(
          onPressed: () => this.onItemPressed(context),
          icon: Icon(Icons.arrow_back),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(-0.01413, 0.51498),
              end: Alignment(1.01413, 0.48502),
              stops: [
                0,
                1,
              ],
              colors: [
                Color.fromARGB(255, 90, 90, 90),
                Color.fromARGB(255, 168, 150, 168),
              ],
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: 375,
              height: 150,
              margin: EdgeInsets.only(top: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      height: 15,
                      child: Opacity(
                        opacity: 0.4,
                        child: Text(
                          "Added Photos\n",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: AppColors.primaryText,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            letterSpacing: -0.07385,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: 130,
                    margin: EdgeInsets.only(left: 4, top: 5),
                    child: ListView(
                      physics: BouncingScrollPhysics(),
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                      scrollDirection: Axis.horizontal,
                      children: _photos
                          .map((image) => ViewItemWidget(image: image))
                          .toList(),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 200,
              height: 54,
              margin: EdgeInsets.only(top: 11),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.black, width: 3)),
                onPressed: () => this.onButtonPressed(context),
                color: Color.fromARGB(255, 255, 255, 255),
                padding: EdgeInsets.all(0),
                child: Text(
                  "Add More Photos",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0),
                    fontFamily: "Verdana",
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                  margin: EdgeInsets.only(left: 40, top: 30),
                  child: CouncilFinder()),
            ),
            Container(
              width: 337,
              height: 238,
              margin: EdgeInsets.only(top: 12),
              decoration: BoxDecoration(
                color: AppColors.primaryBackground,
                boxShadow: [Shadows.primaryShadow],
                borderRadius: Radii.k2pxRadius,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    margin: const EdgeInsets.only(right: 10.0, left: 10.0),
                    child: TextFormField(
                      controller: nameController,
                      decoration: const InputDecoration(
                        icon: Icon(Icons.person),
                        hintText: 'Please enter your name!',
                        labelText: 'Name *',
                      ),
                    ),
                  ),
                  Opacity(
                    opacity: 0.1,
                    child: Container(
                      height: 3,
                      margin: EdgeInsets.only(top: 10, right: 2),
                      decoration:
                          BoxDecoration(color: AppColors.secondaryElement),
                      child: Container(),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(right: 10.0, left: 10.0),
                    child: TextFormField(
                      controller: issueController,
                      decoration: const InputDecoration(
                        icon: Icon(Icons.error),
                        hintText: 'What is your issue?',
                        labelText: 'Issue *',
                      ),
                    ),
                  ),
                  Opacity(
                    opacity: 0.1,
                    child: Container(
                      height: 3,
                      margin: EdgeInsets.only(top: 10, right: 2),
                      decoration:
                          BoxDecoration(color: AppColors.secondaryElement),
                      child: Container(),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(right: 10.0, left: 10.0),
                    child: TextFormField(
                        controller: additionalDetailsController,
                        decoration: const InputDecoration(
                          icon: Icon(Icons.insert_comment),
                          hintText: 'Have any additional comments?',
                          labelText: 'Additional Details',
                        ),
                        maxLines: 2),
                  ),
                ],
              ),
            ),
            Container(
              width: 200,
              height: 54,
              margin: EdgeInsets.only(top: 40),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.black, width: 3)),
                onPressed: () {
                  final String name = nameController.text;
                  final String issue = issueController.text;
                  final String additionalDetails =
                      additionalDetailsController.text;
                  submitIssue(name, issue, additionalDetails);
                  onButtonTwoPressed(context); //
                },
                color: Color.fromARGB(255, 255, 255, 255),
                padding: EdgeInsets.all(0),
                child: Text(
                  "Submit",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0),
                    fontFamily: "Verdana",
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
