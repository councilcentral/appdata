/**
 *  view_item_widget.dart
 *  CouncilCentral
 *
 *  Created by ApplicationsTeam.
 *  Copyright © 2020 CouncilCentral. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'dart:io';

class ViewItemWidget extends StatelessWidget {
  final File image;

  ViewItemWidget({Key key, @required this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 4.0, right: 4.0),
      width: 122,
      height: 122,
      child: Image.file(this.image, fit: BoxFit.cover)
    );
  }
}