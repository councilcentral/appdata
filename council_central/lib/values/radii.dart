/*
*  radii.dart
*  CouncilCentral
*
*  Created by ApplicationsTeam.
*  Copyright © 2020 CouncilCentral. All rights reserved.
    */

import 'package:flutter/rendering.dart';


class Radii {
  static const BorderRadiusGeometry k2pxRadius = BorderRadius.all(Radius.circular(2));
}