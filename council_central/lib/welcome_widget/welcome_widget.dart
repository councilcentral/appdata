/**
 *  welcome_widget.dart
 *  CouncilCentral
 *
 *  Created by ApplicationsTeam.
 *  Copyright © 2020 CouncilCentral. All rights reserved.
 */

import 'package:council_central/report_issue_detail_widget/report_issue_detail_widget.dart';
import 'package:council_central/report_list_widget/report_list_widget.dart';
import 'package:council_central/welcome_widget/welcome_widget_animation1_element3.dart';
import 'package:council_central/welcome_widget/welcome_widget_animation1_element4.dart';
import 'package:council_central/welcome_widget/welcome_widget_animation1_element5.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
import 'dart:io';

class WelcomeWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _WelcomeWidgetState();
}

class _WelcomeWidgetState extends State<WelcomeWidget> with TickerProviderStateMixin {
  AnimationController councilCentralTextAnimationController;
  AnimationController oneStopReportingAppTextAnimationController;
  AnimationController newReportButtonAnimationController;
  AnimationController reportStatusButtonAnimationController;
  AnimationController councilCentralTwoTextAnimationController;

  @override
  void initState() {
    super.initState();
    this.newReportButtonAnimationController =
        AnimationController(duration: Duration(milliseconds: 950), vsync: this);
    this.reportStatusButtonAnimationController =
        AnimationController(duration: Duration(milliseconds: 950), vsync: this);
    this.councilCentralTwoTextAnimationController =
        AnimationController(duration: Duration(milliseconds: 950), vsync: this);

    this.startAnimationOne();
  }

  @override
  void dispose() {
    super.dispose();

    this.councilCentralTextAnimationController.dispose();
    this.oneStopReportingAppTextAnimationController.dispose();
    this.newReportButtonAnimationController.dispose();
    this.reportStatusButtonAnimationController.dispose();
    this.councilCentralTwoTextAnimationController.dispose();
  }

  void onSignUpPressed(BuildContext context) => Navigator.push(
      context, MaterialPageRoute(builder: (context) => ReportListWidget()));

  void newReportPressed(BuildContext context) {
    if (this._takenPhoto != null) {
      final MaterialPageRoute route = MaterialPageRoute(
        builder: (context) => ReportIssueDetailWidget(takenPhoto: this._takenPhoto)
      );
      Navigator.push(context, route);
    }
  }

  void startAnimationOne() {
    this.newReportButtonAnimationController.forward();
    this.reportStatusButtonAnimationController.forward();
  }

  File _takenPhoto; // TODO: Use this variable in report_issue_detail_widget.dart.
  
  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _takenPhoto = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            children: [
              // Logo
              Container(
                height: 100,
                margin: EdgeInsets.only(top: 50, left: 20),
                child: Image.asset("assets/images/logoinverted.png", fit: BoxFit.fill)
              ),
              Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 10)),
              // New Report Container
              Container(
                width: 300,
                margin: EdgeInsets.only(bottom: 24),
                child: WelcomeWidgetAnimation1Element3(
                  animationController: this.newReportButtonAnimationController,
                  child: MaterialButton(
                    onPressed: () {
                      getImage().then((ignored) => {
                        newReportPressed(context),
                      });
                    },
                    padding: EdgeInsets.all(0),
                    child: Stack(
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.circular(40.0),
                          child: Image.asset("assets/images/NewReport.png", scale: 0.1),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                            child: Text(
                              "New Report",
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "Verdana",
                                fontWeight: FontWeight.w700,
                                fontSize: 28,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 10)),
              // Report Status
              Container(
                width: 300,
                height: 250,
                margin: EdgeInsets.only(bottom: 15),
                child: WelcomeWidgetAnimation1Element4(
                  animationController: this.reportStatusButtonAnimationController,
                  child: FlatButton(
                    onPressed: () => this.onSignUpPressed(context),
                    padding: EdgeInsets.all(0),
                    child: Stack(
                      children: <Widget>[
                        Center(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(40.0),
                            child: Image.asset("assets/images/ReportStatus.png", scale: 0.1)
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                            child: Text(
                              "Report Status",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "Verdana",
                                fontWeight: FontWeight.w700,
                                fontSize: 28,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              // Copyright
              Container(
                margin: EdgeInsets.only(bottom: 0),
                child: WelcomeWidgetAnimation1Element5(
                  animationController: this.councilCentralTwoTextAnimationController,
                  child: Text(
                    "© 2020 Council Central",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Verdana",
                      fontWeight: FontWeight.w400,
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
