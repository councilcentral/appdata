/*
*  news_item_widget.dart
*  CouncilCentral
*
*  Created by ApplicationsTeam.
*  Copyright © 2020 CouncilCentral. All rights reserved.
    */

import 'package:council_central/values/values.dart';
import 'package:flutter/material.dart';


class NewsItemWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  
    return Container(
      constraints: BoxConstraints.expand(height: 419),
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Positioned(
            left: 0,
            top: 30,
            right: 0,
            bottom: 59,
            child: Container(
              decoration: BoxDecoration(
                color: AppColors.primaryBackground,
              ),
              child: Container(),
            ),
          ),
          Positioned(
            left: 0,
            top: 80,
            right: 0,
            bottom: 81,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: 200,
                  child: Image.asset(
                    "assets/images/bg-news-temp-2.png",
                    fit: BoxFit.fill,
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    margin: EdgeInsets.only(left: 20, top: 18, right: 20),
                    child: Text(
                      "As seen above, the contractor has arrived and is\nrepairing the picnic bench.",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Color.fromARGB(255, 7, 7, 7),
                        fontFamily: "Lato",
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        height: 1.42857,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            left: 9,
            top: 14,
            right: 10,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 82,
                  height: 82,
                  child: Image.asset(
                    "assets/images/icon-avatar.png",
                    fit: BoxFit.none,
                  ),
                ),
                Container(
                  width: 142,
                  height: 32,
                  margin: EdgeInsets.only(left: 8, top: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Cumberland Council",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color.fromARGB(255, 15, 15, 15),
                            fontFamily: "Verdana",
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          margin: EdgeInsets.only(top: 3),
                          child: Opacity(
                            opacity: 0.4,
                            child: Text(
                              "Today, 1:45 PM",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: AppColors.primaryText,
                                fontFamily: "Verdana",
                                fontWeight: FontWeight.w400,
                                fontSize: 10,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}