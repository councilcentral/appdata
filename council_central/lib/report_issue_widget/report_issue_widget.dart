/*
*  report_issue_widget.dart
*  CouncilCentral
*
*  Created by ApplicationsTeam.
*  Copyright © 2020 CouncilCentral. All rights reserved.
    






    // This widget/screen will be depreciated soon as it is no longer needed - these functions are old code used for testing of the image picker system.







import 'package:council_central/report_issue_detail_widget/report_issue_detail_widget.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
import 'dart:io';
class UploadImage extends StatefulWidget {
  @override
  _UploadImageState createState() => _UploadImageState();
}

class _UploadImageState extends State<UploadImage> {
  void onLoginPressed(BuildContext context) => Navigator.push(context, MaterialPageRoute(builder: (context) => ReportIssueDetailWidget()));
  
  void onGroupPressed(BuildContext context) => Navigator.pop(context);
  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {
       return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Text(
          "Take Photo",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Color.fromARGB(255, 255, 255, 255),
            fontFamily: "Verdana",
            fontWeight: FontWeight.w400,
            fontSize: 17,
        ),
        ),
       leading: IconButton(
          onPressed: () => this.onGroupPressed(context),
          icon: Image.asset("assets/images/group-2.png",),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(-0.01413, 0.51498),
              end: Alignment(1.01413, 0.48502),
              stops: [
                0,
                1,
              ],
              colors: [
                Color.fromARGB(255, 90, 90, 90),
                Color.fromARGB(255, 168, 150, 168),
              ],
            ),
          ),
        ),
      ),
      body: 
      Center(
        child: _image == null
            ? Text('No image selected.')
            : Image.file(_image),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getImage,
        tooltip: 'Pick Image',
        backgroundColor: Colors.black,
        child: Icon(Icons.add_a_photo),
      ),
      floatingActionButtonLocation:    
      FloatingActionButtonLocation.centerFloat,
 body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment(0.31089, 1.09827),
            end: Alignment(0.68911, -0.09827),
            stops: [
              0,
              1,
            ],
            colors: [
              Color.fromARGB(255, 0, 0, 0),
              Color.fromARGB(255, 255, 255, 255),
            ],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              width: 200,
              height: 60,
              margin: EdgeInsets.only(bottom: 70),
              child: FlatButton(
                onPressed: getImage,
                color: Color.fromARGB(255, 255, 255, 255),
                padding: EdgeInsets.all(0),
                child: Text(
                  "Take Photo",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color.fromARGB(255, 35, 31, 31),
                    fontFamily: "Verdana",
                    fontWeight: FontWeight.w700,
                    fontSize: 15,
                  ),
                ),
              ),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                  color: Colors.black,
                  width: 3,
              ),
            ),
            ),
          ],
        ),
      ),
    
    );
  }
}


class ReportIssueWidget extends StatelessWidget {
  




  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Text(
          "Take Photo",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Color.fromARGB(255, 255, 255, 255),
            fontFamily: "Verdana",
            fontWeight: FontWeight.w400,
            fontSize: 17,
          ),
        ),
        leading: IconButton(
          onPressed: () => this.onGroupPressed(context),
          icon: Image.asset("assets/images/group-2.png",),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(-0.01413, 0.51498),
              end: Alignment(1.01413, 0.48502),
              stops: [
                0,
                1,
              ],
              colors: [
                Color.fromARGB(255, 90, 90, 90),
                Color.fromARGB(255, 168, 150, 168),
              ],
            ),
          ),
        ),
      ),

      
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment(0.31089, 1.09827),
            end: Alignment(0.68911, -0.09827),
            stops: [
              0,
              1,
            ],
            colors: [
              Color.fromARGB(255, 0, 0, 0),
              Color.fromARGB(255, 255, 255, 255),
            ],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              width: 200,
              height: 60,
              margin: EdgeInsets.only(bottom: 42),
              child: FlatButton(
                onPressed: () => this.onLoginPressed(context),
                color: Color.fromARGB(255, 255, 255, 255),
                padding: EdgeInsets.all(0),
                child: Text(
                  "Take Photo",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color.fromARGB(255, 35, 31, 31),
                    fontFamily: "Verdana",
                    fontWeight: FontWeight.w700,
                    fontSize: 15,
                  ),
                ),
              ),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                  color: Colors.black,
                  width: 3,
              ),
            ),
            ),
          ],
        ),
      ),
    );
  }
} */
