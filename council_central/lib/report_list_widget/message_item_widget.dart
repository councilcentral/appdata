/*
*  message_item_widget.dart
*  CouncilCentral
*
*  Created by ApplicationsTeam.
*  Copyright © 2020 CouncilCentral. All rights reserved.
    */

import 'package:council_central/current_status_widget/current_status_widget.dart';
import 'package:flutter/material.dart';


class MessageItemWidget extends StatelessWidget {

  void onItemPressed(BuildContext context) => Navigator.push(context, MaterialPageRoute(builder: (context) => CurrentStatusWidget()));
  
  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: () => this.onItemPressed(context),
      child: Container(
        constraints: BoxConstraints.expand(height: 80),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Row(
          children: [
            Container(
              width: 62,
              height: 62,
              margin: EdgeInsets.only(left: 14),
              child: Image.asset(
                "assets/images/avatar-temp-4.png",
                fit: BoxFit.none,
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                width: 163,
                height: 35,
                margin: EdgeInsets.only(left: 9, top: 23),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Broken Picnic Bench",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 0, 0),
                          fontFamily: "Verdana",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(top: 4),
                        child: Text(
                          "Being Reviewed by X Council",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color.fromARGB(255, 148, 148, 148),
                            fontFamily: "Verdana",
                            fontWeight: FontWeight.w400,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Spacer(),
            Container(
              width: 7,
              height: 13,
              margin: EdgeInsets.only(right: 20),
              child: Image.asset(
                "assets/images/path-2.png",
                fit: BoxFit.none,
              ),
            ),
          ],
        ),
      ),
    );
  }
}