/*
*  confirmation_submit_widget.dart
*  CouncilCentral
*
*  Created by ApplicationsTeam.
*  Copyright © 2020 CouncilCentral. All rights reserved.
    */

import 'package:council_central/report_list_widget/report_list_widget.dart';
import 'package:council_central/welcome_widget/welcome_widget.dart';
import 'package:flutter/material.dart';


class ConfirmationSubmitWidget extends StatelessWidget {
  
  void onButtonTwoPressed(BuildContext context) => Navigator.push(context, MaterialPageRoute(builder: (context) => WelcomeWidget()));
  
  void onButtonThreePressed(BuildContext context) => Navigator.push(context, MaterialPageRoute(builder: (context) => ReportListWidget()));
  
  void onGroupPressed(BuildContext context) => Navigator.pop(context);
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Text(
          "Submitted!",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Color.fromARGB(255, 255, 255, 255),
            fontFamily: "Verdana",
            fontWeight: FontWeight.w400,
            fontSize: 17,
          ),
        ),
        leading: IconButton(
          onPressed: () => this.onGroupPressed(context),
          icon: Icon(Icons.arrow_back),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(-0.01413, 0.51498),
              end: Alignment(1.01413, 0.48502),
              stops: [
                0,
                1,
              ],
              colors: [
                Color.fromARGB(255, 90, 90, 90),
                Color.fromARGB(255, 168, 150, 168),
              ],
            ),
          ),
        ),
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 244, 242, 244),
        ),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 150),
              child: Text(
                "Your issue has been submitted!",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontFamily: "Verdana",
                  fontWeight: FontWeight.w400,
                  fontSize: 28,
                ),
              ),
            ),

            Container(
                  height: 200,
                  margin: EdgeInsets.only(top: 60),
                  child: Image.asset(
                    "assets/images/tick.png",
                    fit: BoxFit.fill,
                  ),
                ),


            Container(
              width: 200,
              height: 54,
              margin: EdgeInsets.only(top: 125),
              child: FlatButton(
                onPressed: () => this.onButtonTwoPressed(context),
                color: Colors.white,
                shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
                side: BorderSide(color: Colors.black, width: 3)
                ),
                padding: EdgeInsets.all(0),
                child: Text(
                  "Home",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0),
                    fontFamily: "Verdana",
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                  ),
                ),
              ),
            ),
            
            
            Container(
              width: 200,
              height: 54,
              margin: EdgeInsets.only(top: 29),
              child: FlatButton(
                onPressed: () => this.onButtonThreePressed(context),
                color: Colors.white,
                shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
                side: BorderSide(color: Colors.black, width: 3)
                ),
                padding: EdgeInsets.all(0),
                child: Text(
                  "Report Status",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0),
                    fontFamily: "Verdana",
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}