/*

This file has the relevant code for finding the council region given a set of
GPS coordinates.

Written by: Adam van Zuylen
Date: 04/05/2020
 */

import 'dart:async';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';

class CouncilFinder extends StatelessWidget {
  Future<List<Placemark>> get placemarks async {
    Geolocator geolocator = Geolocator();
    Position position = await geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    print("latitude");
    print(position.latitude);
    print("longitude");
    print(position.longitude);

    return geolocator.placemarkFromPosition(position);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Placemark>>(
      future: this.placemarks,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          // Future completed with an error.
          return Text(
            snapshot.error.toString(),
            textAlign: TextAlign.left,
            style: TextStyle(
              color: Color.fromARGB(255, 0, 0, 0),
              fontFamily: "Verdana",
              fontWeight: FontWeight.w400,
              fontSize: 18,
            ),
          );
        } else if (snapshot.hasData) {
          // Future completed with a value.
          String council = snapshot.data.isEmpty
            ? "No council detected"
            : "Council Detected: ${snapshot.data.first.subAdministrativeArea}";

          return Text(
            council,
            textAlign: TextAlign.left,
            style: TextStyle(
              color: Color.fromARGB(255, 0, 0, 0),
              fontFamily: "Verdana",
              fontWeight: FontWeight.w400,
              fontSize: 18,
            ),
          );
        } else {
          // Uncompleted.
          return Text(
            'Detecting Council',
            textAlign: TextAlign.left,
            style: TextStyle(
              color: Color.fromARGB(255, 0, 0, 0),
              fontFamily: "Verdana",
              fontWeight: FontWeight.w400,
              fontSize: 18,
            ),
          );
        }
      },
    );
  }
}




//final coordinates = new Coordinates(1.10, 45.50);
//addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
//first = addresses.first;
//print("${first.featureName} : ${first.addressLine}");

//My Function:
