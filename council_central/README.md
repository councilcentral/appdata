# Council Central

This is the Council Central app source code.

## FAQ:
Read below to understand what's going on in this repo.

## What is .gitignore?
This is in place so binary files, that are unique to each persons device, don't get uploaded to the bitbucket, as it'll cause build problems for others.